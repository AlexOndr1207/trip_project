 let quiz1=[
            {
                toAsk:"В каком дворце живёт королева Великой Британии?",
                answers:["Букингемский", "Вестминстерский", "Кенсингтонский", "Ламбетский"]
            },
            {
                toAsk:"Над какой рекой расположен Тауэрский мост?",
                answers:["Темза", "Дунай", "Днепр", "Родинг"]
            },
            {
                toAsk:"Над какой рекой расположен Тауэрский мост?",
                answers:["Бэйкер-Стрит", "Кингс Роуд", "Оксфорд Стрит", "Пиккадилли"]
            },
            {
                toAsk:"Какое официальное название известнейшей часовой башни Лондона?",
                answers:["Елизаветинская башня", "Биг-Бен", "Вестминстерская башня", "Часовая башня"]
            }
        ]

        const test1 = document.getElementById('test1');
		const resultsDiv = document.getElementById('results');
		let completed = false

		let score = 0;

		function random(arr){
			let n = Math.floor(Math.random()*arr.length);
			return arr[n]
		}
		function mixAnsw(arr){
			let newArr =[];
			let startArr = arr;
			for (let i=0; i<4; i++) {
				let rand = random(arr);
				newArr.push(rand);
				let n = arr.indexOf(rand);
				arr.splice(n, 1);
			}
			//console.log(startArr, newArr);
			return newArr
		}
		function decideQuestions(arr1){
			let task1 =[];
			for (let i=0; i<4; i++) {
				let randObj = random(arr1)
				task1[i] = randObj;
				task1[i].answers = mixAnsw(task1[i].answers);
				arr1.splice(arr1.indexOf(randObj), 1);
				/*if (task1[i].toAsk==task1[i-1].toAsk) {
					while (task1[i].toAsk==task1[i-1].toAsk) {
						task1[i] = random(arr1);
						task1[i].answers = mixAnsw(task1[i].answers)
					}
				}*/
				console.log(task1, arr1)
			}
			
			//console.log(task1);
			
            complete = appendTest(task1);
            return completed
		}
		
		function checkAnsw(btn, task) {
			if (btn.innerHTML == task.right) {
				score++;
				btn.classList.add('correct')
			} else {
				//console.log(btn.innerHTML, task.right)
				btn.classList.add('wrong')
			}
			
			//console.log(score)
			
		}
		
		function appendTest(task1){
			task1.forEach((q, i) => {
				let div1 = document.createElement('div');
				div1.classList.add('task1')
				div1.id=`task1q${i}`;
				let title = document.createElement('h2');
				title.innerHTML = q.toAsk;
				let answDiv = document.createElement('div');
				answDiv.id = `answDiv${i}`;
				q.answers.forEach(a => {
					let answBtn = document.createElement('button');
					answBtn.innerHTML = a;
					answDiv.appendChild(answBtn);
					//console.log(q.right, answBtn)
					answBtn.addEventListener('click', ()=>{
						checkAnsw(answBtn, q);
						//console.log(document.querySelector(`.task1`));
						document.querySelectorAll(`#answDiv${i} button`).forEach(item =>{
							item.disabled = true;
						})
						let allBtnsDisabled = true;
						document.querySelectorAll(`.task1 button`).forEach(btn =>{
							if (!btn.disabled) allBtnsDisabled = false
						})
						if (allBtnsDisabled){
							completed = calcScore()
							
						}
					})
				});
				div1.appendChild(title);
				div1.appendChild(answDiv);
				
				test1.appendChild(div1);
                return completed
			});
			
				/*
<input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter">
    <label for="subscribeNews">Subscribe to newsletter?</label>

				*/
				
		}
		
		function calcScore(){
			let coinsRes = score*25;
            coins+=coinsRes;
            console.log(coins)
            sessionStorage.coins = coins;
			let procents = score*25+'%';
			let res = '';
			if (score>=3) {
				res = 'Тест зарахований!';
				completed = true;
                switch (testName) {
                    case 1:
                      sessionStorage.test1 = true
                      break;
                    case 2:
                    sessionStorage.test2 = true
                      break;
                      case 3:
                      sessionStorage.test3 = true
                      break;
                    case 4:
                    sessionStorage.test4 = true
                      break;
                      case 5:
                      sessionStorage.test5 = true
                      break;
                    
                    
                  }
			} else {
				res = 'Цього недостатьно, пройдіть тест знову!'
				completed = false;
			}
			resultsDiv.innerHTML =
			`<h2>Ви прошли тест на ${procents} та отримали ${coinsRes} <img src="images/coin.png" alt="Монетка" width="20px" height="20px">. ${res}</h2>`;
			console.log(completed);
            return completed
		}
        